// Array

let grades = [98.5, 94.3, 89.2, 90.1];

console.log(grades[0]);


// ---------------------------------------------------------------

/*
	An objects is similar to array.
	It is collection of related data and or functionalities. usually it represent real world object.
*/

let grade = {
	//Object initializer/ literal notation
	//  - objects consist of properties, which are used to describe an object. key-value pair.
	math: 89.2,
	english: 98.5,
	science: 90.1,
	filipino: 94.3
}
console.log(grade.english);

/*
	We used dot notation to access the properties/value of our object.

	Syntax: 

		let objectName = {
			keyA: valueA,
			keyB: valueB
		}

*/ 


let cellphone = {
	brandName: 'Nokia 3310',
	color: 'Dark Blue',
	manufacturerDate: 1999
}
console.log(typeof cellphone);

let student = {
	firstName: 'John',
	lastName: 'Smith',
	location: {
		city: 'Tokyo',
		country: 'Japan'

	},
	email: ['john@mail.com', 'johnsmith@mail.xyz'],
	// Object method - function inside object
	fullName: function(){
		return this.firstName + ' ' +
			this.lastName
	}

}
// Best Practice for Object
console.log(student.location.city);
// bracket notation 
console.log(student['firstName']); 

// function call
console.log(student.email);
console.log(student.email[0]);

console.log(student.fullName());


const person1 = {
	name: 'Jane',
	greeting: function(){
		return 'Hi I\'m ' + this.name
	}
}
console.log(person1.greeting())

// ---------------------------------------------------------------------

/*
	ARRAY OF OBJECTS
*/

let contactList = [
	{
		firstName: 'John',
		lastName: 'Smith',
		location: 'Japan'
	},
	{
		firstName: 'Jane',
		lastName: 'Smith',
		location: 'Japan'
	},
	{
		firstName: 'Jasmine',
		lastName: 'Smith',
		location: 'Japan'
	}


]

console.log(contactList[0].firstName)

// Object literals
let people = [
	{
		name: 'Juanita',
		age: 13
	},
	{
		name: 'Juanito',
		age: 14
	}

]
people.forEach(function(person){
	console.log(person.name)
})

// 
console.log(`${people[0].name} are the list.`)


// Creating a object using a Constractor Function(JS OBJECT CONSTRUCTOR/OBJECT FROM BLUEPRINT)

// Creates a reussable function to vreate severel objects that have the same data structures

// this is useful for creating multipe copies/instances of an object

// Object Literals
// let object ={}

// Instance - distance/unique objects
// let object = new object
// -an instance is a concrete occurence of any objecet which emphasizes on the uniqueidentity of it

/*
	Syntax:

		function objectName(key!, keyB){
			this.keeyA = keyA,
			this.keeyB = keyB,

		}
*/ 

function Laptop(name, manufactureDate){
	// The 'this' keyword allows to asign a nnew oobjects property by associating them with values received from our parameter
	this.name = name,
	this.manufactureDate = manufactureDate
}

// This is a unique instance of the laptop object
let laptop = new Laptop('Lenovo', 2008)
console.log('Result from cereating objects using object constructor');
console.log(laptop)


// This is another unique instance of the laptop object
let myLaptop = new Laptop('MacBook Air', 2020)
console.log('Result from cereating objects using object constructor');
console.log(myLaptop)

// 
let oldLaptop = Laptop('Portal R2E CCMC', 1980)
console.log('without new keyword')
console.log(oldLaptop)


// Creating empty objects
// literals
let computer = {}
// instance
let myComputer = new Object();

// .notatiion
console.log(myLaptop.name);

// brackeet notation
console.log(myLaptop['name']);



let array = [laptop, myLaptop]

console.log(array[0].name)

// ------------------------------------------------------------------------
/*
	Initiali0zing/adding/deleting/reassigning/ objectProperties
	
	Initialized/Added 
		properties after the oobject was created
		This is useful for times wwhen an objects properties are undermined at the time of creating them


*/

let car = {}

// Add an object properties, we can use dot notation.
car.name = 'Honda Civic'
console.log(car)


car['manufactureDate'] = 2019;
console.log(car)

// Reasigning objects properties
car.name = 'Dodge Charger R/T'
console.log(car)


// Deleting Object peroperties
delete car['manufactureDate']
console.log('Result from deleting properties')
console.log(car)


/*
	OBJECT METHOD

		 A method is a function which is a property of an object

		 They are also functions and one of the key differences they have iis that methods are function

*/

let person = {
	name: 'John',
	talk: function(){
		console.log('Hello my name is ' + this.name)
	}
}
console.log(person)
console.log('Result from objject methods')
person.talk()



// Adding methods to object person
person.walk = function(){
	console.log(this.name + ' walked 25 steps forward')
}

person.walk()



let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address:{
		city: 'Austin',
		country: 'Texas'
	},
	email: ['john@mail.com', 'johnsmith@mail.xyz'],
	introduce: function(){
		console.log('Hello my name is ' + this.firstName + ' ' + this.lastName)
	}
}
friend.introduce()


/*
	real World Application of Objects
	-Scenario

		1. We would like to create a game that would have several pokemon

*/


let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log('This pokemon tackled target Pokemon')
		console.log("'targetPokemon's healyh is now reduced too _targetPokemonHealth_")

	},
	faint: function(){
		console.log('Pokemon fainted')
	}
}


function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methds

	this.tackle = function(target){
		console.log(this.name + ' tackled' + target.name)
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	};
	this.faint = function(){
		console.log(this.name + 'fainted')
	}
}
let pikachu = new Pokemon('Pikachu', 16)
let charizard = new Pokemon('Charizard', 8)

pikachu.tackle(charizard)

